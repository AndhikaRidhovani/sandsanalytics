/*! [PROJECT_NAME] | Suitmedia */

;(function ( window, document, undefined ) {

    var path = {
        css: myPrefix + 'assets/css/',
        js : myPrefix + 'assets/js/vendor/'
    };

    var assets = {
        _jquery_cdn     : 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.1/jquery.min.js',
        _jquery_local   : path.js + 'jquery.min.js',
        _fastclick      : path.js + 'fastclick.min.js',
        _slick          : path.js + 'slick.min.js',
        _headroom       : path.js + 'headroom.min.js',
        _jquery_nav     : path.js + 'jquery.nav.min.js',
        _baze_validate  : path.js + 'baze.validate.min.js'
    };

    var Site = {

        init: function () {
            Site.fastClick();
            Site.enableActiveStateMobile();
            Site.WPViewportFix();
            Site.mainSlider();
            Site.productSlider();
            Site.hofSlider();
            Site.navBar();
            Site.pageNav();
            Site.dropdownMenu();
            Site.validation();

            window.Site = Site;
        },

        fastClick: function () {
            Modernizr.load({
                load    : assets._fastclick,
                complete: function () {
                    FastClick.attach(document.body);
                }
            });
        },

        enableActiveStateMobile: function () {
            if ( document.addEventListener ) {
                document.addEventListener('touchstart', function () {}, true);
            }
        },

        WPViewportFix: function () {
            if ( navigator.userAgent.match(/IEMobile\/10\.0/) ) {
                var style   = document.createElement("style"),
                    fix     = document.createTextNode("@-ms-viewport{width:auto!important}");

                style.appendChild(fix);
                document.getElementsByTagName('head')[0].appendChild(style);
            }
        },

        mainSlider: function () {
            Modernizr.load({
                load    :assets._slick,
                complete: function () {
                    $('.main-slider').slick({
                        dots: true,
                        accessibility: false,
                        autoplay: true,
                        autoplaySpeed: 3000
                    });
                }
            });
        },

        productSlider: function () {
            Modernizr.load({
                load    :assets._slick,
                complete: function () {
                    $('.products-slider').slick({
                        dots: true,
                        accessibility: false,
                        autoplay: true,
                        autoplaySpeed: 3000
                    });
                }
            });
        },

        hofSlider: function () {
            Modernizr.load({
                load    :assets._slick,
                complete: function () {
                    $('.team-slider').slick({
                        accessibility: false,
                        centerMode: true,
                        centerPadding: '30%',
                        slidesToShow: 1,
                        prevArrow: '<button type="button" class="slick-prev fa fa-angle-left fa-5x"></button>',
                        nextArrow: '<button type="button" class="slick-next fa fa-angle-right fa-5x"></button>',
                        responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                          }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                          }
                        }
                      ]
                    });
                }
            });
        },

        navBar: function () {
            Modernizr.load({
                load    :assets._headroom,
                complete: function () {
                    var header = document.querySelector('header');
                    var headroom = new Headroom(header, {
                          "offset": 600,
                          "tolerance": 5,
                          "classes": {
                            "initial": "animated",
                            "pinned": "slideDown",
                            "unpinned": "slideUp"
                          }
                    });
                    headroom.init();
                }
            });
        },

        pageNav: function () {
            Modernizr.load({
                load    :assets._jquery_nav,
                complete: function () {
                    $('.nav-bar').onePageNav({
                        currentClass: 'current',
                        changeHash: false,
                        scrollSpeed: 750,
                        scrollThreshold: 0.5,
                        filter: '',
                        easing: 'swing',
                        begin: function() {
                            //I get fired when the animation is starting
                        },
                        end: function() {
                            //I get fired when the animation is ending
                        },
                        scrollChange: function($currentListItem) {
                            //I get fired when you enter a section and I pass the list item of the section
                        }
                    });
                }
            });
        },

        dropdownMenu: function () {
            var btn = $('.dropdown-menu');
            var navbar = $('.nav-bar');
            btn.on('click', function(event) {
                navbar.toggle();
            });
        },

        validation: function () {
            Modernizr.load({
                load    :assets._baze_validate,
                complete:function (){
                     $('form').bazeValidate();
                }
            })
        }
    };

    var checkJquery = function () {
        Modernizr.load([
            {
                test    : window.jQuery,
                nope    : assets._jquery_local,
                complete: Site.init
            }
        ]);
    };

    Modernizr.load({
        load    : assets._jquery_local,
        complete: checkJquery
    });

})( window, document );
